(ns nao.output)

(defn entry-str [entry]
  (let [{[hh mm] :time
         task :task} entry
        time (str hh ":" mm)]
    (str time " " task)))

(defn day-lines [day]
  (if (empty? (:entries day))
    ["No entries"]
    (map entry-str (:entries day))))
