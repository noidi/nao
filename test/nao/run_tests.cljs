(ns nao.run-tests
  (:require-macros [expectations.cljs :refer [run-all-tests]])
  (:require doo.runner
            expectations
            nao.core-test
            nao.fs-test
            nao.db-test
            nao.output-test
            nao.edn-test
            nao.time-test
            nao.acceptance.add-test
            nao.acceptance.duration-test))

(doo.runner/set-entry-point! (fn [] (run-all-tests)))
