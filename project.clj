(defproject nao "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.854"]
                 [com.andrewmcveigh/cljs-time "0.5.0"]
                 [instaparse "1.4.7"]]
  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[com.cemerick/piggieback "0.2.2"]
                                  [org.clojure/tools.nrepl "0.2.10"]
                                  [expectations "2.2.0-beta2"]
                                  [org.clojure/test.check "0.10.0-alpha2"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}}
  :plugins [[lein-cljsbuild "1.1.6"]
            [lein-doo "0.1.7"]
            [lein-shell "0.5.0"]]
  :clean-targets ["target" "bin"]
  :cljsbuild {:builds [{:id "prod"
                        :source-paths ["src"]
                        :compiler {:output-to "bin/nao.js"
                                   :output-dir "target/prod/"
                                   :main nao.core
                                   :target :nodejs
                                   :optimizations :simple
                                   :static-fns true
                                   :optimize-constants true}}
                       {:id "dev"
                        :source-paths ["src"]
                        :compiler {:output-to "target/dev/nao.js"
                                   :output-dir "target/dev/"
                                   :main nao.core
                                   :target :nodejs
                                   :optimizations :none}}
                       {:id "test"
                        :source-paths ["src" "test"]
                        :compiler {:output-to "target/test/run.js"
                                   :output-dir "target/test"
                                   :main nao.run-tests
                                   :target :nodejs
                                   :optimizations :none}}]}
  :aliases {"test" ["doo" "node" "test"]
            "build-dev" ["cljsbuild" "auto" "dev"]
            "build-prod" ["do" "clean,"
                          "cljsbuild" "once" "prod,"
                          "shell" "chmod" "+x" "bin/nao.js"]})
