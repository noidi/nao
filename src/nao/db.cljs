(ns nao.db
  (:require cljs-time.format
            nao.fs))

(def shell (js/require "shelljs"))

(defn date-str [date]
  (cljs-time.format/unparse-local-date {:format-str "YYYY-MM-dd"} date))

(defn day-path [db date]
  (let [filename (str (date-str date) ".edn")]
    (nao.fs/make-path db filename)))

(defn init [db]
  (.mkdir shell "-p" db))

(defn read-day [db date]
  (init db)
  (let [path (day-path db date)]
    (if (nao.fs/path-exists? path)
      (assoc (nao.fs/slurp-edn path)
             :date date)
      {:date date
       :entries []})))

(defn write-day [db day]
  (init db)
  (nao.fs/spit-edn (day-path db (:date day))
                   (dissoc day :date))
  day)

(defn update-day [db date f & args]
  (let [day (read-day db date)]
    (write-day db (apply f day args))))
