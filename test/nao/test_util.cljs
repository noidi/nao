(ns nao.test-util
  (:require-macros [nao.test-util :refer [with-temp-dir]])
  (:require clojure.string
            [expectations :refer-macros [more-of]
             :refer [CustomPred]]))

(def fs (js/require "fs"))
(def path (js/require "path"))
(def shell (js/require "shelljs"))

(defn make-temp-dir []
  (.mkdtempSync fs (str (.tempdir shell) (.-sep path) "nao-")))

(defn exception [re]
  (more-of e
    js/Error e
    re (.-message e)))

(defrecord TestCheckSuccess []
  CustomPred
  (expect-fn [e a] (:result a))
  (expected-message [e a str-e str-a]
    (when-not (instance? js/Error a)
      (str "Failed with size " (:failing-size a) " after " (:num-tests a) " tests")))
  (actual-message [e a str-e str-a]
    (if (instance? js/Error a)
      a
      (str "fail: " (:fail a))))
  (message [e a str-e str-a]
    (when-not (instance? js/Error a)
      (str "shrunk: " (get-in a [:shrunk :smallest])))))

(def test-check-success (->TestCheckSuccess))
