(ns nao.output-test
  (:require [expectations :refer-macros [expect]]
            cljs-time.core
            nao.output))

(expect
  "12:34 foo bar"
  (nao.output/entry-str {:time [12 34], :task "foo bar"}))

(expect
  ["No entries"]
  (nao.output/day-lines {:date #nao/Date "2001-02-03"
                         :entries []}))

(expect
  ["12:34 foo bar"
   "14:56 baz xyzzy"]
  (nao.output/day-lines {:date #nao/Date "2001-02-03"
                         :entries [{:time [12 34], :task "foo bar"}
                                   {:time [14 56], :task "baz xyzzy"}]}))

