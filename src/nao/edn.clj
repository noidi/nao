(ns nao.edn)

(defn edn->local-date [s]
  `(edn->local-date ~s))

(defn edn->local-date-time [s]
  `(edn->local-date-time ~s))
