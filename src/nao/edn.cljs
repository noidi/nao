(ns nao.edn
  (:require cljs.reader
            cljs-time.format))

;;; goog.date.Date

(def local-date-formatter (cljs-time.format/formatter "yyyy-MM-dd"))

(defn local-date->edn [date]
  (str "#nao/Date \"" (cljs-time.format/unparse-local-date local-date-formatter date) \"))

(defn edn->local-date [s]
  (cljs-time.format/parse-local-date local-date-formatter s))

(extend-protocol IPrintWithWriter
  goog.date.Date
  (-pr-writer [date out opts]
    (-write out (local-date->edn date))))

;;; goog.date.DateTime

(def local-date-time-formatter (cljs-time.format/formatter "yyyy-MM-dd'T'HH:mm"))

(defn local-date-time->edn [date]
  (str "#nao/DateTime \"" (cljs-time.format/unparse-local local-date-time-formatter date) \"))

(defn edn->local-date-time [s]
  (cljs-time.format/parse-local local-date-time-formatter s))

(extend-protocol IPrintWithWriter
  goog.date.DateTime
  (-pr-writer [date out opts]
    (-write out (local-date-time->edn date))))
