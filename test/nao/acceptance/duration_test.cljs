(ns nao.acceptance.duration-test
  (:require [expectations :refer-macros [expect more->]]
            [nao.acceptance.util :refer [nao]]
            [nao.test-util :refer-macros [with-temp-dir]]))

;; A duration should be printed after each entry
#_(expect
  (more->
    0 :exit
    #"\[00:30]$" (-> :lines rest (nth 0))
    #"\[02:00]$" (-> :lines rest (nth 1)))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:00"}
         "add foo @ 9:30"
         "add bar @ 10:00"
         "")))
