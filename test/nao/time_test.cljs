(ns nao.time-test
  (:require-macros clojure.test.check.properties)
  (:require cljs.reader
            [clojure.test.check :as test.check]
            [clojure.test.check.generators :as generators]
            [clojure.test.check.properties :as properties]
            [expectations :refer-macros [expect]]
            [nao.test-util :refer [test-check-success]]
            nao.time))

(expect
  "23:09"
  (str (nao.time/make 23 9)))

(expect
  "#nao/Time \"23:09\""
  (pr-str (nao.time/make 23 9)))

(expect
  (nao.time/make 23 9)
  (cljs.reader/read-string "#nao/Time \"23:09\""))

(def prop-edn-roundtrip
  (properties/for-all [hours (generators/choose 0 23)
                       minutes (generators/choose 0 59)]
    (let [in (nao.time/make hours minutes)
          out (cljs.reader/read-string (pr-str in))]
      (= in out))))

(expect
  test-check-success
  (test.check/quick-check 100 prop-edn-roundtrip))
