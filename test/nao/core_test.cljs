(ns nao.core-test
  (:require [expectations :refer-macros [expect]]
            [nao.test-util :refer [exception]]
            cljs-time.core
            nao.core))

(expect
  {:command :show
   :date #nao/Date "2017-01-01"
   :time [12 0]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  ""))

(expect
  {:command :show
   :date #nao/Date "2017-01-01"
   :time [12 0]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "show"))

(expect
  {:command :show
   :date #nao/Date "2015-10-10"
   :time [12 0]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10"))

(expect
  (exception #"2015-60-10")
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-60-10"))

(expect
  {:command :show
   :date #nao/Date "2015-10-10"
   :time [12 0]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10 show"))

(expect
  {:command :add
   :task "foo bar"
   :date #nao/Date "2017-01-01"
   :time [12 0]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "add foo bar"))

(expect
  {:command :add
   :task "foo bar"
   :date #nao/Date "2017-01-01"
   :time [15 32]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "add foo bar@15:32"))

(expect
  {:command :add
   :task "foo bar"
   :date #nao/Date "2017-01-01"
   :time [15 32]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "add foo bar @ 15:32"))

(expect
  {:command :add
   :task "foo bar"
   :date #nao/Date "2015-10-10"
   :time [12 0]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10 add foo bar"))

(expect
  {:command :add
   :task "foo bar"
   :date #nao/Date "2015-10-10"
   :time [15 32]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10 add foo bar@15:32"))

(expect
  {:command :add
   :task "foo bar"
   :date #nao/Date "2015-10-10"
   :time [15 32]}
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10 add foo bar @ 15:32"))

(expect
  (exception #"25:32")
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10 add foo bar @ 25:32"))

(expect
  (exception #"15:60")
  (nao.core/parse #nao/DateTime "2017-01-01T12:00"
                  "2015-10-10 add foo bar @ 15:60"))

