(ns nao.test-util)

(defmacro with-temp-dir [let-name & body]
  `(let [~let-name (make-temp-dir)]
     (try
       ~@body
       (finally
         (.rm shell "-rf" ~let-name)))))
