(ns nao.fs
  (:require cljs.reader
            cljs.pprint))

(def fs (js/require "fs"))
(def path (js/require "path"))

(defn path-exists? [path]
  (try
    (.accessSync fs path)
    true
    (catch js/Error _
      false)))

(defn make-path [& components]
  (clojure.string/join (.-sep path) components))

(defn spit [path data]
  (.writeFileSync fs path data))

(defn slurp [path]
  (.readFileSync fs path "utf8"))

(defn spit-edn [path data]
  (spit path (with-out-str (cljs.pprint/pprint data))))

(defn slurp-edn [path]
  (cljs.reader/read-string (slurp path)))
