(ns nao.core
  (:require cljs.nodejs
            clojure.string
            cljs-time.core
            cljs-time.coerce
            cljs-time.extend
            cljs-time.format
            [instaparse.core :refer-macros [defparser]]
            nao.fs
            nao.db
            nao.output
            nao.edn))

(cljs.nodejs/enable-util-print!)

(defparser parser
  "
  command-line = date | command | date <whitespace> command
  command = show | add
  show = '' | <'show'>
  add = <'add'> <whitespace> task (<whitespace>? <'@'> <whitespace>? time)?
  whitespace = #'\\s+'
  task = #'[^@:]+'
  date = iso-8601-date
  iso-8601-date = #'[0-9]{4}-[0-9]{2}-[0-9]{2}'
  time = hh <':'> mm
  hh = #'[0-9]{1,2}'
  mm = #'[0-9]{1,2}'
  ")

(def parse-tree-transforms
  {:command-line merge
   :command merge
   :show (constantly {:command :show})
   :add #(apply merge {:command :add} %&)
   :task #(hash-map :task (clojure.string/trim %))
   :date #(hash-map :date %)
   :iso-8601-date (fn [date-str]
                    (if-let [date (cljs-time.format/parse-local-date date-str)]
                      date
                      (throw (js/Error. (str "Invalid date: " date-str)))))
   :time (fn [hh mm]
           (if (and (<= 0 hh 23)
                    (<= 0 mm 59))
             {:time [hh mm]}
             (throw (js/Error. (str "Invalid time: " hh ":" mm)))))
   :hh #(js/parseInt % 10)
   :mm #(js/parseInt % 10)})

(defn parse [now command-line]
  (let [parse-tree (parser command-line)]
    (if (instaparse.core/failure? parse-tree)
      parse-tree
      (merge
        {:date (cljs-time.coerce/to-local-date now)
         :time [(cljs-time.core/hour now) (cljs-time.core/minute now)]
         :command :show}
        (instaparse.core/transform parse-tree-transforms parse-tree)))))

(defmulti run-command (fn [db command] (:command command)))

(defmethod run-command :add [db command]
  (let [entry (select-keys command [:task :time])]
    (nao.db/update-day db (:date command) update :entries conj entry))
  (run-command db (assoc command :command :show)))

(defmethod run-command :show [db command]
  (let [day (nao.db/read-day db (:date command))]
    {:exit 0
     :lines (into ["Header"] (nao.output/day-lines day))}))

(defn run [db now & args]
  (let [command (parse now (clojure.string/join " " args))]
    (if (instaparse.core/failure? command)
      {:exit 1
       :lines (clojure.string/split-lines (pr-str command))}
      (run-command db command))))

(defn -main [& args]
  (let [db (or js/process.env.NAO_DB
               (nao.fs/make-path js/process.env.HOME ".nao" "db"))
        now (cljs-time.core/to-default-time-zone (cljs-time.core/now))
        {:keys [lines exit]} (apply run db now args)]
    (doseq [line lines]
      (println line))
    (.exit js/process exit)))

(set! *main-cli-fn* -main)
