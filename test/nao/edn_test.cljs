(ns nao.edn-test
  (:require cljs.reader
            [expectations :refer-macros [expect]]
            cljs-time.core
            nao.edn))

;; Print LocalDate.
(expect
  "#nao/Date \"2001-02-03\""
  (pr-str (cljs-time.core/local-date 2001 2 3)))

;; Read LocalDate.
(expect
  (cljs-time.core/local-date 2001 2 3)
  (cljs.reader/read-string "#nao/Date \"2001-02-03\""))

;; Round-trip LocalDate.
(expect
  (cljs-time.core/local-date 2001 2 3)
  (-> (cljs-time.core/local-date 2001 2 3)
    pr-str
    cljs.reader/read-string))

;; Use LocalDate in source code.
(expect
  (cljs-time.core/local-date 2001 2 3)
  #nao/Date "2001-02-03")

;; Print LocalDateTime.
(expect
  "#nao/DateTime \"2001-02-03T14:15\""
  (pr-str (cljs-time.core/local-date-time 2001 2 3 14 15)))

;; Read LocalDateTime.
(expect
  (cljs-time.core/local-date-time 2001 2 3 14 15)
  (cljs.reader/read-string "#nao/DateTime \"2001-02-03T14:15\""))

;; Round-trip LocalDateTime.
(expect
  (cljs-time.core/local-date-time 2001 2 3 14 15)
  (-> (cljs-time.core/local-date-time 2001 2 3 14 15)
    pr-str
    cljs.reader/read-string))

;; Use LocalDateTime in source code.
(expect
  (cljs-time.core/local-date-time 2001 2 3 14 15)
  #nao/DateTime "2001-02-03T14:15")

