(ns nao.db-test
  (:require [expectations :refer-macros [expect]]
            cljs-time.core
            [nao.test-util :refer-macros [with-temp-dir]]
            nao.db
            nao.fs))

;; DB initialization creates the DB directory. Initialization is idempotent.
(expect
  (with-temp-dir db-parent
    (let [db (nao.fs/make-path db-parent "foo" "bar")]
      (nao.db/init db)
      (nao.db/init db)
      (nao.fs/path-exists? db))))

;; read-day returns an empty day if the day has not been written.
(expect
  {:date #nao/Date "2001-02-03"
   :entries []}
  (with-temp-dir db
    (nao.db/read-day db #nao/Date "2001-02-03")))

;; read-day returns whatever was written for that day with write-day.
(expect
  {:date #nao/Date "2001-02-03"
   :entries [:some {'random "EDN data"}]}
  (with-temp-dir db
    (nao.db/write-day db {:date #nao/Date "2001-02-03"
                          :entries [:some {'random "EDN data"}]})
    (nao.db/read-day db #nao/Date "2001-02-03")))

;; write-day returns the day that was written.
(expect
  {:date #nao/Date "2001-02-03"
   :entries [:some {'random "EDN data"}]}
  (with-temp-dir db
    (nao.db/write-day db {:date #nao/Date "2001-02-03"
                          :entries [:some {'random "EDN data"}]})))

;; update-day updates a day's data with the given fn and arguments.
(expect
  {:date #nao/Date "2001-02-03"
   :entries [:entry]}
  (with-temp-dir db
    (nao.db/update-day db #nao/Date "2001-02-03"
                       update :entries conj :entry)
    (nao.db/read-day db #nao/Date "2001-02-03")))

;; update-day returns the updated day.
(expect
  {:date #nao/Date "2001-02-03"
   :entries [:entry]}
  (with-temp-dir db
    (nao.db/update-day db #nao/Date "2001-02-03"
                       update :entries conj :entry)))

