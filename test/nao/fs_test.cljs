(ns nao.fs-test
  (:require [expectations :refer-macros [expect]]
            [nao.test-util :refer-macros [with-temp-dir]]
            nao.fs))

;; slurp throws if the path doesn't exist.
(expect
  js/Error
  (with-temp-dir d
    (let [f (nao.fs/make-path d "foo.txt")]
      (nao.fs/slurp f))))

;; slurp returns whatever data was spit to the file.
(expect
  "Lorem ipsum"
  (with-temp-dir d
    (let [f (nao.fs/make-path d "foo.txt")]
      (nao.fs/spit f "Lorem ipsum")
      (nao.fs/slurp f))))

;; slurp-edn throws if the path doesn't exist.
(expect
  js/Error
  (with-temp-dir d
    (let [f (nao.fs/make-path d "foo.txt")]
      (nao.fs/slurp-edn f))))

;; slurp-edn returns whatever data was spit to the file with spit-edn.
(expect
  [:some {'random "EDN data"}]
  (with-temp-dir d
    (let [f (nao.fs/make-path d "foo.txt")]
      (nao.fs/spit-edn f [:some {'random "EDN data"}])
      (nao.fs/slurp-edn f))))
