(ns nao.acceptance.add-test
  (:require [expectations :refer-macros [expect more->]]
            [nao.acceptance.util :refer [nao]]
            [nao.test-util :refer-macros [with-temp-dir]]))

;; No entries should be listed if add isn't called.
(expect
  (more->
    0 :exit
    ["No entries"] (-> :lines rest))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:34"}
         "")))

;; If no time is specified, the entry is added with the current time.
(expect
  (more->
    0 :exit
    ["12:34 Hello, World!"] (-> :lines rest))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:34"}
         "add Hello, World!")))

;; If a time is specified, the entry is added with the specified time.
(expect
  (more->
    0 :exit
    ["16:20 Hello, World!"] (-> :lines rest))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:34"}
         "add Hello, World! @ 16:20")))

;; If no date is specified, the entry is added for the current day.
(expect
  (more->
    0 :exit
    ["12:34 Hello, World!"] (-> :lines rest))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:34"}
         "add Hello, World!"
         "2017-01-01 show")))

;; If a date is specified, the entry is added for the specified day.
(expect
  (more->
    0 :exit
    ["12:34 bar"] (-> :lines rest))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:34"}
         "2017-02-01 add foo"
         "2017-02-02 add bar"
         "2017-02-02 show")))

;; If another date is specified, the current day is not modified.
(expect
  (more->
    0 :exit
    ["No entries"] (-> :lines rest))
  (with-temp-dir db
    (nao db {:on "2017-01-01" :at "12:34"}
         "2017-02-02 add Hello, World!"
         "show")))
