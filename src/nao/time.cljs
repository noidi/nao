(ns nao.time
  (:require-macros instaparse.core)
  (:require [instaparse.core :as instaparse]))

;; Nao represents all times and durations as the number of minutes since
;; something (i.e. times are just the number of minutes since midnight).

(defn padded-str [n]
  (if (< n 10)
    (str "0" n)
    n))

(defrecord Time [minutes]
  Object
  (toString [t]
    (let [hours (quot (:minutes t) 60)
          minutes (rem (:minutes t) 60)]
      (str (padded-str hours) \: (padded-str minutes)))))

(defn make
  ([hours minutes]
   (->Time (+ (* 60 hours) minutes)))
  ([minutes]
   (->Time minutes)))

(defn time->edn [t]
  (str "#nao/Time \"" t \"))

;; The grammar for EDN time strings is stricter than what is accepted on the
;; command line.
(instaparse/defparser parse-edn-time-str
  "
  time = hours <':'> minutes
  hours = <'0'> #'[0-9]' | '1' #'[0-9]' | '2' #'[0-3]'
  minutes = <'0'> #'[0-9]' | #'[1-5]' #'[0-9]'
  ")

(defn edn->time [s]
  (->> s
    parse-edn-time-str
    (instaparse/transform {:hours (comp cljs.reader/read-string str)
                           :minutes (comp cljs.reader/read-string str)
                           :time (fn [hours minutes]
                                   (make hours minutes))})))

(extend-protocol IPrintWithWriter
  nao.time.Time
  (-pr-writer [t out opts]
    (-write out (time->edn t))))
