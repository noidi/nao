(ns nao.acceptance.util
  (:require cljs-time.format
            nao.core))

(defn nao
  "Runs nao as if invoked on the given date and time with the given commands.
  Returns the result of the last command."
  [db {:keys [on at]} & commands]
  (let [datetime-str (str on "T" at)
        datetime (cljs-time.format/parse-local datetime-str)]
    (last (doall (for [command commands]
                   (nao.core/run db datetime command))))))
